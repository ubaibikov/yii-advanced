<?php

namespace api\repositroies\contracts;

interface ApiContract
{
    /**
     * Get all rws
     * 
     * @param int $id rw identifier
     */
    public function all(int $id);

    /**
     * Get detail/current rws
     * 
     * @param int $id rw identifer
     * @param int $currentId current rw identifier
     */
    public function detail(int $id, int $currentId);

    /**
     * Create for rw identifier
     * 
     * @param int $id rw identifier
     * @param array $data rw created data
     */
    public function create(int $id, array $data);

    /**
     * Udpate detail/current rws
     * 
     * @param int $currentId current rw identifier
     * @param array $data current rw updated data
     */
    public function update(int $currentId, array $data);

    /**
     * Udpate detail/current rws
     * 
     * @param int $currentId current rw identifier
     */
    public function delete(int $currentId);
}
