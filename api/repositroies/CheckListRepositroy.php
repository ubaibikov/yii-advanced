<?php

namespace api\repositroies;

use api\repositroies\contracts\ApiContract;
use api\modules\v1\models\CheckList;

class CheckListRepositroy implements ApiContract
{
    /**
     * @property ActiveRecord $checkList CheckList model 
     */
    private $checkList;

    public function __construct(CheckList $checkList)
    {
        $this->checkList = $checkList;
    }

    /**
     * Get all checklists where user
     * 
     * @param int $id User identifier
     * @return model
     */
    public function all(int $id)
    {
        return $this->checkList::find()
            ->where(['user_id' => $id])
            ->all();
    }

    /**
     * Get detail checklist 
     * 
     * @param int $id User identifier 
     * @param int $currentId  CheckList identifier
     * @return model
     */
    public function detail(int $id, int $currentId)
    {
        return $this->checkList::find()
            ->where(['user_id' => $id, 'id' => $currentId])
            ->one();
    }

    /**
     * Create user checklist
     * 
     * @param int $id $userId
     * @param array $data checkList created data
     * @return boolean
     */
    public function create(int $id, array $data)
    {
        $checkList = new $this->checkList;
        $checkList->user_id = $id;  // because we have small data .. 
        $checkList->title = $data['title'];

        if ($checkList->validate()) {
            return $checkList->save();
        }

        return false;
    }

    /**
     * Update user checklist 
     * 
     * @param int $currentId Cheklist identifier
     * @param array $data cheklist updated data
     * @return
     */
    public function update(int $currentId, array $data)
    {
        return $this->checkList::updateAll($data, ['id' => $currentId]);
    }

    /**
     * Delete user checklist 
     * 
     * @param int $currentId Cheklist identifier
     * @return boolean
     */
    public function delete(int $currentId)
    {
        $deletedChecklist = $this->checkList::findOne($currentId);
        return $deletedChecklist->delete();
    }
}
