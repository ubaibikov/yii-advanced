<?php

namespace api\common\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;
use Yii;


class BasicApiController extends ActiveController
{

    use \api\common\traits\ApiResponse;

    /**
     * if we use custom actions like this api actions..
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index'],
        $actions['view'],
        $actions['create'],
        $actions['update'],
        $actions['delete']);
    }

    /**
     * Middleware for format etc.
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }
}
