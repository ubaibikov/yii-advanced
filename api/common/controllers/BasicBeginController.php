<?php

namespace api\common\controllers;

use api\common\controllers\BasicApiController;
use yii\filters\auth\{CompositeAuth,HttpBearerAuth};

/**
 * Extending Basic controller for auths users
 */
class BasicBeginController extends BasicApiController
{
    /**
     * Middleware for auths
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBearerAuth::class,
            ],
        ];

        return $behaviors;
    }
}
