<?php

namespace api\common\traits;

trait ApiResponse
{
    /**
     * Send current response 
     * 
     * @param int $status Response status code
     * @param mixed $data Response data
     * @return Response \Yii::$app->response
     */
    protected function sendResponse(int $status, $data)
    {
        $response = \Yii::$app->response;
        $response->statusCode = $status;
        $response->format = $this->getJsonFormat();
        $response->data = $data;

        return $response;
    }

    /**
     * Get current format
     * 
     * @return FORMAT_JSON
     */
    private function getJsonFormat()
    {
        return \yii\web\Response::FORMAT_JSON;
    }
}
