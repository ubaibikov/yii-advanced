<?php

namespace api\modules\v1\models;

use yii\db\ActiveRecord;
use yii\db\ActiveRecordInterface;
use api\modules\v1\models\Point;

class CheckList extends ActiveRecord implements ActiveRecordInterface
{
    public static function tableName()
    {
        return 'check_lists';
    }
    
    public function fields()
    {
        return [
            'id',
            'title',
            'points',
            'performedPoints',
            'unperformedPoints'    
        ];
    }

    public function rules()
    {
        return [
            [['title'] , 'required'],
        ];
    }

    public function getPoints()
    {
        return $this->hasMany(Point::class, ['check_list_id' => 'id']);
    }

    public function getPerformedPoints()
    {
        return $this->perfomedPoints(1);
    }

    public function getUnperformedPoints()
    {
        return $this->perfomedPoints(0);
    }

    protected function perfomedPoints(int $perfomed)
    {
        return $this->hasMany(Point::class,['check_list_id' => 'id'])
            ->where(['performed' => $perfomed]);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        Point::deleteAll(['check_list_id' => $this->id]);
    }

    public static function primaryKey()
    {
        return ['id'];
    }
}
