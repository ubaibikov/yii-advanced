<?php

namespace api\modules\v1\models;

use yii\db\ActiveRecord;
use yii\db\ActiveRecordInterface;

class Point extends ActiveRecord implements ActiveRecordInterface
{
    public static function tableName()
    {
        return 'points';
    }
    public function fields()
    {
        return [
            'title',
            'id',
        ];
    }
    public static function primaryKey()
    {
        return 'id';
    }
}
