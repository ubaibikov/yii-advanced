<?php

namespace api\modules\v1\controllers;

use api\common\controllers\BasicBeginController;
use api\repositroies\contracts\ApiContract;

/**
 * Checklist Controller Api
 */
class CheckListController extends BasicBeginController
{
    //бесполезно все работает через repository
    public $modelClass = 'common\models\User';

    public function actionGet(int $userId, ApiContract $checkList)
    {
        $checkLists = $checkList->all($userId);
        
        return $this->sendResponse(200, $checkLists);
    }

    public function actionDetail(int $userId, int $checkListId, ApiContract $checkList)
    {
        $checklist = $checkList->detail($userId, $checkListId);

        return $this->sendResponse(200, $checklist);
    }

    public function actionCreate(int $userId, ApiContract $checkList)
    {
        $request = \Yii::$app->request;
        $createNewCheckList = $checkList->create($userId, ['title' => $request->post('title')]);

        if ($createNewCheckList) {
            return $this->sendResponse(201, 'OK');
        }

        return $this->sendResponse(404, 'Dont Created');
    }

    public function actionUpdate(int $checkListId, ApiContract $checkList)
    {
        $request = \Yii::$app->request;
        $updateCheckList = $checkList->update($checkListId, $request->post());

        return  $this->sendResponse(200, ['updated count' => $updateCheckList]);
    }

    public function actionDelete(int $checkListId, ApiContract $checkList)
    {
        $deleteCheckList = $checkList->delete($checkListId);

        return $this->sendResponse(200, $deleteCheckList);
    }
}
