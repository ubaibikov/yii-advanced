<?php

namespace api\modules\v1\controllers;

use api\common\controllers\BasicApiController;
use common\models\LoginForm;
use common\models\RegisterForm;

class AuthController extends BasicApiController
{
    public $modelClass = 'common\models\User';

    public function actionRegister()
    {
        $request = \Yii::$app->request;
        $register = new RegisterForm();

        $register->username = $request->post('username');
        $register->email = $request->post('email');
        $register->password = $request->post('password');

        if ($accessToken = $register->register()) {
            return $this->sendResponse(200, ['Access Token' => $accessToken]);
        }

        return $this->sendResponse(422, 'FALSE');
    }
}
