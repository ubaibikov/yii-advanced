<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    /**
     * Set namespace
     */
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'container' => [
        'singletons' => [
            'api\repositroies\contracts\ApiContract' => ['class' => 'api\repositroies\CheckListRepositroy'],
        ],
    ],
    /**
     * Set current version modules
     */
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module',
        ],
    ],
    'components' => [
        'request' => [
            // 'cookieValidationKey' => '3213e2dqewdw',
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableSession' => false
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                /**
                 * Routes
                 */
                [
                    /**
                     * Checklists
                     */
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/check-list',
                    'extraPatterns' => [
                        'GET <userId>' => 'get',
                        'GET <userId>/<checkListId>' => 'detail',
                        'POST <userId>' => 'create',
                        'PATCH update/<checkListId>' => 'update',
                        'DELETE delete/<checkListId>' => 'delete',
                    ],
                ],
                [
                    /**
                     * Auth
                     */
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/auth',
                    'extraPatterns' => [
                        'POST register' => 'register',
                    ],
                    'pluralize' => false,
                ]
            ],
        ],
    ],
    'params' => $params,
];
