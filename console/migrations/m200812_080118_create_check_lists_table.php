<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%check_lists}}`.
 */
class m200812_080118_create_check_lists_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%check_lists}}', [
            'id'      => $this->primaryKey(),
            'user_id' => $this->integer(),
            'title'   => $this->string(55),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%check_lists}}');
    }
}
