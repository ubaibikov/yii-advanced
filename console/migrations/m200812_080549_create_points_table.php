<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%points}}`.
 */
class m200812_080549_create_points_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%points}}', [
            'id' => $this->primaryKey(),
            'check_list_id' => $this->integer(),
            'title' => $this->string(55),
            'performed' => $this->boolean()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%points}}');
    }
}
